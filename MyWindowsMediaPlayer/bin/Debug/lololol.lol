<?xml version="1.0"?>
<ArrayOfMedia xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Media>
    <SupportImage>
      <string>.jpg</string>
      <string>.png</string>
      <string>.bmp</string>
      <string>.jpeg</string>
      <string>.JPG</string>
    </SupportImage>
    <SuppVideo>
      <string>.wmv</string>
      <string>.mp4</string>
      <string>.avi</string>
    </SuppVideo>
    <SuppMusic>
      <string>.mp3</string>
      <string>.aac</string>
      <string>.ogg</string>
      <string>.wav</string>
      <string>.flac</string>
    </SuppMusic>
    <MediaName>Get Lucky (Official Audio)</MediaName>
    <FilePath>file:///C:/Users/user/Music/Get Lucky (Official Audio).mp3</FilePath>
    <InitialFileName>C:\Users\user\Music\Get Lucky (Official Audio).mp3</InitialFileName>
    <Extension>.mp3</Extension>
    <mediaType>MUSIC</mediaType>
    <trackNb>0</trackNb>
    <artists />
    <genres />
    <secondes>8</secondes>
    <minutes>4</minutes>
    <hours>0</hours>
  </Media>
  <Media>
    <SupportImage>
      <string>.jpg</string>
      <string>.png</string>
      <string>.bmp</string>
      <string>.jpeg</string>
      <string>.JPG</string>
    </SupportImage>
    <SuppVideo>
      <string>.wmv</string>
      <string>.mp4</string>
      <string>.avi</string>
    </SuppVideo>
    <SuppMusic>
      <string>.mp3</string>
      <string>.aac</string>
      <string>.ogg</string>
      <string>.wav</string>
      <string>.flac</string>
    </SuppMusic>
    <MediaName>Amber Run - I Found</MediaName>
    <FilePath>file:///C:/Users/user/Music/Amber Run - I Found.mp3</FilePath>
    <InitialFileName>C:\Users\user\Music\Amber Run - I Found.mp3</InitialFileName>
    <Extension>.mp3</Extension>
    <mediaType>MUSIC</mediaType>
    <trackNb>0</trackNb>
    <artists />
    <genres />
    <secondes>35</secondes>
    <minutes>4</minutes>
    <hours>0</hours>
  </Media>
  <Media>
    <SupportImage>
      <string>.jpg</string>
      <string>.png</string>
      <string>.bmp</string>
      <string>.jpeg</string>
      <string>.JPG</string>
    </SupportImage>
    <SuppVideo>
      <string>.wmv</string>
      <string>.mp4</string>
      <string>.avi</string>
    </SuppVideo>
    <SuppMusic>
      <string>.mp3</string>
      <string>.aac</string>
      <string>.ogg</string>
      <string>.wav</string>
      <string>.flac</string>
    </SuppMusic>
    <MediaName>Jamaican in New York - Shinehead (Lyrics)</MediaName>
    <FilePath>file:///C:/Users/user/Music/Jamaican in New York - Shinehead (Lyrics).mp3</FilePath>
    <InitialFileName>C:\Users\user\Music\Jamaican in New York - Shinehead (Lyrics).mp3</InitialFileName>
    <Extension>.mp3</Extension>
    <mediaType>MUSIC</mediaType>
    <trackNb>0</trackNb>
    <artists />
    <genres />
    <secondes>49</secondes>
    <minutes>3</minutes>
    <hours>0</hours>
  </Media>
  <Media>
    <SupportImage>
      <string>.jpg</string>
      <string>.png</string>
      <string>.bmp</string>
      <string>.jpeg</string>
      <string>.JPG</string>
    </SupportImage>
    <SuppVideo>
      <string>.wmv</string>
      <string>.mp4</string>
      <string>.avi</string>
    </SuppVideo>
    <SuppMusic>
      <string>.mp3</string>
      <string>.aac</string>
      <string>.ogg</string>
      <string>.wav</string>
      <string>.flac</string>
    </SuppMusic>
    <MediaName>BOOOM! Étienne Chouard brise l'omertà en direct à la télé!!!</MediaName>
    <FilePath>file:///C:/Users/user/Videos/BOOOM! Étienne Chouard brise l'omertà en direct à la télé!!!.mp4</FilePath>
    <InitialFileName>C:\Users\user\Videos\BOOOM! Étienne Chouard brise l'omertà en direct à la télé!!!.mp4</InitialFileName>
    <Extension>.mp4</Extension>
    <mediaType>VIDEO</mediaType>
    <trackNb>0</trackNb>
    <artists />
    <genres />
    <secondes>41</secondes>
    <minutes>11</minutes>
    <hours>0</hours>
  </Media>
</ArrayOfMedia>