﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Windows;

namespace MyWindowsMediaPlayer
{
    class Libraries
    {
        public List<string> SavedLibraryPath;

        public Libraries()
        {
            SavedLibraryPath = new List<string>();
            if (File.Exists(Properties.Resources.SavedLibraryPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<string>));
                FileStream stream = File.OpenRead(Properties.Resources.SavedLibraryPath);

                SavedLibraryPath = (List<string>)serializer.Deserialize(stream);
                stream.Close();
            }
        }

        public void AddLibrary(string path)
        {
            if (path != null)
            {
                SavedLibraryPath.Add(path);
            }
            else
                MessageBox.Show("null path.");
        }

        public void SaveLibraries()
        {
            if (System.IO.Directory.Exists(Properties.Resources.PlayListFolderPath) == false)
            {
                System.IO.Directory.CreateDirectory(Properties.Resources.PlayListFolderPath);
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<string>));
            FileStream stream = File.Open(Properties.Resources.SavedLibraryPath, FileMode.Create, FileAccess.Write, FileShare.None);

            serializer.Serialize(stream, SavedLibraryPath);
            stream.Close();
        }

        public void CheckLibraryPaths()
        {
            foreach (string s in SavedLibraryPath)
            {
                if (!System.IO.Directory.Exists(s))
                {
                    SavedLibraryPath.Remove(s);
                    MessageBox.Show("A Folder was not found. It was deleted from PathList.");
                }

            }
        }
    }
}
