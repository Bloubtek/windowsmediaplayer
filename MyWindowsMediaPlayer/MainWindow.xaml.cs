﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using System.Xml.Linq;

namespace MyWindowsMediaPlayer
{
    public partial class MainWindow : Window
    {
        public enum EMediaStreamStatus { NOTHING, PLAY, PAUSE };
        public enum ELectureTypeStatus { NOLOOP, LOOPALL, LOOPONE };
        public enum EOrderBy { ASC, DESC };

        EMediaStreamStatus MediaStatus = EMediaStreamStatus.NOTHING;
        Boolean PlaylistRand = false;
        ELectureTypeStatus LectureStatus;
        Playlist currentPlaylist;
        string[] PlaylistsPaths;
        DispatcherTimer _timer = new DispatcherTimer();
        TimeSpan _position;
        TextBlock currentPosInPlaylist;
        Playlist tmpPlaylistMainScreen;
        EOrderBy OrderByStatus = EOrderBy.ASC;
        Libraries lib = new Libraries();

        public MainWindow()
        {
            InitializeComponent();
            currentPlaylist = new Playlist();
            UpdatePlaylistTreeView();
            currentPosInPlaylist = new TextBlock();
            tmpPlaylistMainScreen = new Playlist();
            VolumeBar.Value = 50;
            LectureStatus = ELectureTypeStatus.NOLOOP;
        }

        private void PlayLoadedMedia()
        {
            PlayPauseImg.ImageSource = new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this), Properties.Resources.PauseButtonPath));
            if ((currentPlaylist.getActualMedia().mediaType == Media.MediaType.VIDEO) || (currentPlaylist.getActualMedia().mediaType == Media.MediaType.IMAGE))
            {
                MainScreen.Visibility = System.Windows.Visibility.Hidden;
                MediaScreen.Visibility = System.Windows.Visibility.Visible;
            }
            else
                MainScreen.Visibility = System.Windows.Visibility.Visible;
            MediaStatus = EMediaStreamStatus.PLAY;
            NameCurrentTrack.Text = currentPlaylist.getActualMedia().MediaName;
            if (currentPlaylist.getActualMedia().imgDatas != null)
                IconCurrentTrack.Source = ByteToImage(currentPlaylist.getActualMedia().imgDatas);
            else
                IconCurrentTrack.Source = null;
            MediaScreen.Play();
            _timer.Start();
        }
        public BitmapImage ByteToImage(byte[] imageData)
        {
            BitmapImage biImg = new BitmapImage();
            MemoryStream ms = new MemoryStream(imageData);
            biImg.BeginInit();
            biImg.StreamSource = ms;
            biImg.EndInit();
            return biImg;
        }
        private void PauseLoadedMedia()
        {
            PlayPauseImg.ImageSource = new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this), Properties.Resources.PlayButtonPath));
            MediaStatus = EMediaStreamStatus.PAUSE;
            MediaScreen.Pause();
            _timer.Stop();
        }
        private void StopLoadedMedia()
        {
            PlayPauseImg.ImageSource = new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this), Properties.Resources.PlayButtonPath));
            MediaStatus = EMediaStreamStatus.PAUSE;
            MediaScreen.Stop();
            _timer.Stop();
        }

        private void UpdatePlaylistTreeView()
        {
            if (System.IO.Directory.EnumerateFiles(Properties.Resources.PlayListFolderPath).Count() > 0)
            {
                int count = 0;
                string tranform;

                PlaylistsPaths = System.IO.Directory.GetFiles(Properties.Resources.PlayListFolderPath);
                TVItemPlaylist.BeginInit();
                TVItemPlaylist.Items.Clear();
                foreach (string s in PlaylistsPaths)
                {
                    if (s.Contains(Properties.Resources.PlaylistExtension))
                    {
                    TreeViewItem tmp = new TreeViewItem();

                    tranform = s;
                    tranform = tranform.Substring(tranform.LastIndexOf("\\") + 1);
                    tranform = tranform.Remove((tranform.Count() - 4));
                    tmp.Header = tranform;
                    tmp.Name = "P" + count.ToString();
                    tmp.MouseDoubleClick += TVIPlaylistDoubleClick;
                    TVItemPlaylist.Items.Add(tmp);
                    }
                    ++count;
                }
                TVItemPlaylist.EndInit();
            }
        }

        private void TVIPlaylistDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var tmp = sender as TreeViewItem;
            int index = 0;

            index = Int32.Parse(tmp.Name.Substring(1, 1));
            currentPlaylist.LoadPlaylist(PlaylistsPaths[index]);
            LoadPlaylistViewFromPlaylist();
            PlaylistName.Text = currentPlaylist.name;
            currentPlaylist.currentlyPlayedIndex = 0;
            currentPosInPlaylist = (TextBlock)CurrentPlaylistList.Children[currentPlaylist.currentlyPlayedIndex];
        }
        private void PlayPauseButton_Click(object sender, RoutedEventArgs e)
        {
            // Here you play the media
            if (MediaStatus == EMediaStreamStatus.NOTHING || MediaStatus == EMediaStreamStatus.PAUSE)
            {
                if (MediaScreen.Source != null)
                    PlayLoadedMedia();
            }
            // here you suspend it
            else if (MediaStatus == EMediaStreamStatus.PLAY)
            {
                if (MediaScreen.Source != null)
                    PauseLoadedMedia();
            }
        }

        private void RandomButton_Click(object sender, RoutedEventArgs e)
        {
            if (PlaylistRand == false)
            {
                PlaylistRand = true;
                RandomButton.Content = "Rand";
            }
            else
            {
                RandomButton.BorderBrush = Brushes.White;
                PlaylistRand = false;
                RandomButton.Content = "NoRand";
                currentPlaylist.currentlyPlayedIndex = 0;
            }
        }

        private void ImportFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            Media tmp = null;
            // Set filter for file extension and default file extension
            dlg.InitialDirectory = Properties.Resources.MusicFolderPath;
            dlg.Filter = getSupportedFilters();


            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                // Open document
                tmp = new Media(dlg.FileName);
                currentPlaylist.add_Element(tmp);
                AddIntoCurrentPlaylist(tmp.MediaName);
            }
        }
        public string getSupportedFilters()
        {
            string Img = "Image Files (" + Properties.Resources.ImageSupport.Replace("*", "") + ")|" + Properties.Resources.ImageSupport;
            string video = "Video Files (" + Properties.Resources.VideoSupport.Replace("*", "") + ")|" + Properties.Resources.VideoSupport;
            string audio = "Audio Files (" + Properties.Resources.AudioSupport.Replace("*", "") + ")|" + Properties.Resources.AudioSupport;
            string finalFilters = Properties.Resources.AllFilesSupport + "|" + audio + "|" + video + "|" + Img;

            return finalFilters;
        }

        private void AddIntoCurrentPlaylist(string name)
        {
            AddTbInGrid(CurrentPlaylistList, name, (currentPlaylist.PlaylistCount() - 1));
            return;
        }

        private void ClickOnItemList(object sender, MouseButtonEventArgs e)
        {
            var tmp = sender as TextBlock;
            Image img = new Image();
            Media mtmp = new Media();

            if (currentPlaylist.getMedia(tmp.Text) != null)
                mtmp = currentPlaylist.getMedia(tmp.Text);
            MediaScreen.Source = mtmp.GetSource();
            MediaScreen.LoadedBehavior = MediaState.Manual;
            MediaScreen.Volume = (int)VolumeBar.Value;
            _timer.Interval = TimeSpan.FromMilliseconds(800);//initial:1000
            _timer.Tick += new EventHandler(ticktock);
            currentPosInPlaylist.Background = CurrentPlaylistList.Background;
            tmp.Background = Brushes.LightBlue;
            TotalTimeMedia.Content = ("0" + mtmp.hours + ((mtmp.minutes > 9 ? ":" + mtmp.minutes : ":0" + mtmp.minutes)) + (mtmp.secondes > 9 ? ":" + mtmp.secondes : ":0" + mtmp.secondes));
            currentPosInPlaylist = tmp;
            if (e.ClickCount >= 2)
            {
                PlayLoadedMedia();
            }
        }

        private void ticktock(object sender, EventArgs e)
        {
            ProgressionMedia.Value = MediaScreen.Position.TotalSeconds;
        }
        private void LoadPlaylistViewFromPlaylist()
        {
            int i = 0;

            CurrentPlaylistList.Children.Clear();
            CurrentPlaylistList.RowDefinitions.Clear();
            foreach (Media media in currentPlaylist.PlDatas)
            {
                    AddTbInGrid(CurrentPlaylistList, media.MediaName, i);
                    ++i;
            }
        }
        private void MediaScreen_MediaEnded(object sender, RoutedEventArgs e)
        {
            var tmp = sender as MediaElement;
            Random rand = new Random();

            currentPosInPlaylist.Background = CurrentPlaylistList.Background;
            if (PlaylistRand)
                currentPlaylist.currentlyPlayedIndex = rand.Next(0, (currentPlaylist.PlaylistCount() - 1));
            switch (LectureStatus)
            {
                case ELectureTypeStatus.NOLOOP:
                    if ((MediaScreen.Source = currentPlaylist.getNextSource(tmp)) == null)
                    {
                        StopLoadedMedia();
                        MediaStatus = EMediaStreamStatus.NOTHING;
                    }
                    else
                    {
                        currentPosInPlaylist = (TextBlock)CurrentPlaylistList.Children[currentPlaylist.currentlyPlayedIndex];
                        currentPosInPlaylist.Background = Brushes.LightBlue;
                        PlayLoadedMedia();
                    }
                    break;
                case ELectureTypeStatus.LOOPALL:
                                    if (currentPlaylist.currentlyPlayedIndex < currentPlaylist.PlaylistCount() - 1)
                    currentPlaylist.currentlyPlayedIndex += 1;
                else
                    currentPlaylist.currentlyPlayedIndex = 0;
                MediaScreen.Source = currentPlaylist.getActualMedia().GetSource();
                currentPosInPlaylist = (TextBlock)CurrentPlaylistList.Children[currentPlaylist.currentlyPlayedIndex];
                currentPosInPlaylist.Background = Brushes.LightBlue;
                PlayLoadedMedia();
                    break;
                case ELectureTypeStatus.LOOPONE:
                    currentPosInPlaylist.Background = Brushes.LightBlue;
                    PauseLoadedMedia();
                    MediaScreen.Stop();
                    PlayLoadedMedia();
                    break;
            }
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            int currentPos;

            if (currentPlaylist.PlaylistCount() > 1 && (MediaScreen.Source != null))
            {
                MediaScreen.Stop();
                if ((MediaScreen.Source = currentPlaylist.getNextSource(MediaScreen)) == null)
                    return;
                currentPosInPlaylist.Background = CurrentPlaylistList.Background;
                currentPos = CurrentPlaylistList.Children.IndexOf(currentPosInPlaylist);
                currentPosInPlaylist = (TextBlock)CurrentPlaylistList.Children[currentPos + 1];
                currentPosInPlaylist.Background = Brushes.LightBlue;
                PlayLoadedMedia();
            }
        }

        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            int currentPos;

            if (currentPlaylist.PlaylistCount() > 1 && (MediaScreen.Source != null))
            {
                MediaScreen.Stop();
                if ((MediaScreen.Source = currentPlaylist.getPrevSource(MediaScreen)) == null)
                    return;
                currentPosInPlaylist.Background = CurrentPlaylistList.Background;
                currentPos = CurrentPlaylistList.Children.IndexOf(currentPosInPlaylist);
                currentPosInPlaylist = (TextBlock)CurrentPlaylistList.Children[currentPos - 1];
                currentPosInPlaylist.Background = Brushes.LightBlue;

                PlayLoadedMedia();
            }

        }

        private void PlaylistName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if ((PlaylistName.Text != null) && (currentPlaylist != null))
            currentPlaylist.name = PlaylistName.Text;
        }

        private void SavePlaylistButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog sfd = new Microsoft.Win32.SaveFileDialog();
            Random rand = new Random();

            if (System.IO.Directory.Exists(Properties.Resources.PlayListFolderPath))
                sfd.InitialDirectory = Properties.Resources.PlayListFolderPath;
            sfd.Filter = Properties.Resources.PlaylistFilter;
            sfd.FileName = (currentPlaylist.name + Properties.Resources.PlaylistExtension);
            sfd.ShowDialog();
            if ((currentPlaylist.name = sfd.FileName) == "")
                currentPlaylist.name = "Playlist" + (rand.Next(520));
            currentPlaylist.SavePlaylist();
            UpdatePlaylistTreeView();
        }
        public void AddTbInGrid(Grid target, string text, int rowPos)
        {
            TextBlock lbl = new TextBlock();
            RowDefinition newRow = new RowDefinition();

            newRow.Height = new GridLength(30);
            target.RowDefinitions.Add(newRow);
            lbl.Name = "Row" + rowPos;
            lbl.Text = text;
            lbl.FontSize = 12;
            lbl.Margin = new Thickness(1, 1, 1, 1);
            if (target.Equals(MainScreen))
            {
                lbl.Background = Brushes.WhiteSmoke;
                lbl.PreviewMouseDown += ClickToDrag;
            }
            else if (target.Equals(CurrentPlaylistList))
            {
                lbl.MouseDown += ClickOnItemList;
            }
            lbl.AllowDrop = true;
            Grid.SetColumn(lbl, 3);
            Grid.SetRow(lbl, rowPos);
            target.Children.Add(lbl);
        }

        private void OnClickedItemKeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                currentPlaylist.RemoveAt(currentPosInPlaylist.Text);
                MediaScreen.Source = currentPlaylist.getPrevSource(MediaScreen);
                //currentPosInPlaylist = (TextBlock)CurrentPlaylistList.Children[currentPlaylist.currentlyPlayedIndex];
                LoadPlaylistViewFromPlaylist();
            }
        }


        // click in textblock when mainscreen != media
        private void ClickToDrag(object sender, MouseEventArgs e)
        {
            var tmp = sender as TextBlock;

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragDropEffects effects;
                DataObject obj = new DataObject();

                obj.SetData(typeof(string), tmp.Text);
                effects = DragDrop.DoDragDrop(tmp, obj, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }

        //Begin drag & drag init from Mainscreen
        private void CurrentPlaylistList_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(string)))
                e.Effects = DragDropEffects.Copy;

            else
                e.Effects = DragDropEffects.None;
        }

        private void CurrentPlaylistListDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(string)))
            {
                string uri = (string)e.Data.GetData(typeof(string));

                e.Effects = DragDropEffects.Copy;
                currentPlaylist.add_Element(tmpPlaylistMainScreen.getMedia(uri));
                AddIntoCurrentPlaylist(uri);
            }

            else
                e.Effects = DragDropEffects.None;
        }

        private void ClickOnArtist(object sender, MouseButtonEventArgs e)
        {
            MainScreen.Background = Brushes.White;
            string[] MusicFiles;
            string[] DirectoriesInFolder;
            int i = 0;
            int totalCount = 0;

            if (System.IO.Directory.Exists(Properties.Resources.MusicFolderPath))
            {
                if ((currentPlaylist.PlDatas.Count() > 0) && (currentPlaylist.getActualMedia().mediaType != Media.MediaType.VIDEO))
                {
                    MediaScreen.Visibility = System.Windows.Visibility.Hidden;
                    MainScreen.Visibility = System.Windows.Visibility.Visible;
                }
                CurrentPathLibrary.Text = Properties.Resources.MusicFolderPath;
                MainScreen.Background = Brushes.White;
                MusicFiles = System.IO.Directory.GetFiles(Properties.Resources.MusicFolderPath);
                DirectoriesInFolder = System.IO.Directory.GetDirectories(Properties.Resources.MusicFolderPath);
                MainScreen.Children.Clear();
                MainScreen.RowDefinitions.Clear();
                while (i < DirectoriesInFolder.Count())
                {
                    if (i > 0)
                        MusicFiles = System.IO.Directory.GetFiles(DirectoriesInFolder[i]);
                    int j = 0;
                    while (j < MusicFiles.Count())
                    {
                        Media media = new Media(MusicFiles[j]);
                        if ((media.mediaType != Media.MediaType.OTHER) && (media.mediaType != Media.MediaType.PLAYLIST))
                        {
                            tmpPlaylistMainScreen.add_Element(media);
                            AddTbInGrid(MainScreen, media.MediaName, totalCount);
                            ++totalCount;
                        }
                        ++j;
                    }
                    ++i;
                }
            }
            else
            {
                MessageBox.Show("The Music Folder doesn't exist.");
                return;
            }
        }

        private void ClickOnAlbum(object sender, MouseButtonEventArgs e)
        {
            MainScreen.Background = Brushes.White;
            string[] MusicFiles;
            string[] DirectoriesInFolder;
            int totalCount = 0;
            int i = 0;

            if (System.IO.Directory.Exists(Properties.Resources.MusicFolderPath))
            {
                if ((currentPlaylist.PlDatas.Count() > 0) && (currentPlaylist.getActualMedia().mediaType != Media.MediaType.VIDEO))
                {
                    MediaScreen.Visibility = System.Windows.Visibility.Hidden;
                    MainScreen.Visibility = System.Windows.Visibility.Visible;
                }
                CurrentPathLibrary.Text = Properties.Resources.MusicFolderPath;
                MainScreen.Background = Brushes.White;
                MusicFiles = System.IO.Directory.GetFiles(Properties.Resources.MusicFolderPath);
                DirectoriesInFolder = System.IO.Directory.GetDirectories(Properties.Resources.MusicFolderPath);
                MainScreen.Children.Clear();
                MainScreen.RowDefinitions.Clear();
                while (i < DirectoriesInFolder.Count())
                {
                    if (i > 0)
                        MusicFiles = System.IO.Directory.GetFiles(DirectoriesInFolder[i]);
                    int j = 0;
                    while (j < MusicFiles.Count())
                    {
                        Media media = new Media(MusicFiles[j]);
                        if ((media.mediaType != Media.MediaType.OTHER) && (media.mediaType != Media.MediaType.PLAYLIST))
                        {
                            tmpPlaylistMainScreen.add_Element(media);
                            AddTbInGrid(MainScreen, media.MediaName, totalCount);
                            ++totalCount;
                        }
                        ++j;
                    }
                    ++i;
                }

                if (tmpPlaylistMainScreen.getActualMedia().album != null)
                {
                    IEnumerable<Media> orderingQuery = null;
                    i = 0;
                    MainScreen.Children.Clear();
                    MainScreen.RowDefinitions.Clear();

                    orderingQuery =
            from media in tmpPlaylistMainScreen.PlDatas
            where media.mediaType == Media.MediaType.MUSIC
            orderby media.album ascending
            select media;
                    OrderByStatus = EOrderBy.ASC;
                    foreach (Media media in orderingQuery)
                    {
                        AddTbInGrid(MainScreen, media.MediaName, i);
                        ++i;
                    }
                }
            }

        }

        private void ClickOnGenre(object sender, MouseButtonEventArgs e)
        {
            MainScreen.Background = Brushes.White;
            string[] MusicFiles;
            int i = 0;
            string[] DirectoriesInFolder;
            int totalCount = 0;

            if (System.IO.Directory.Exists(Properties.Resources.MusicFolderPath))
            {
                if ((currentPlaylist.PlDatas.Count() > 0) && (currentPlaylist.getActualMedia().mediaType != Media.MediaType.VIDEO))
                {
                    MediaScreen.Visibility = System.Windows.Visibility.Hidden;
                    MainScreen.Visibility = System.Windows.Visibility.Visible;
                }
                CurrentPathLibrary.Text = Properties.Resources.MusicFolderPath;
                MainScreen.Background = Brushes.White;
                MusicFiles = System.IO.Directory.GetFiles(Properties.Resources.MusicFolderPath);
                DirectoriesInFolder = System.IO.Directory.GetDirectories(Properties.Resources.MusicFolderPath);
                MainScreen.Children.Clear();
                MainScreen.RowDefinitions.Clear();
                while (i < DirectoriesInFolder.Count())
                {
                    if (i > 0)
                        MusicFiles = System.IO.Directory.GetFiles(DirectoriesInFolder[i]);
                    int j = 0;
                    while (j < MusicFiles.Count())
                    {
                        Media media = new Media(MusicFiles[j]);
                        if ((media.mediaType != Media.MediaType.OTHER) && (media.mediaType != Media.MediaType.PLAYLIST))
                        {
                            tmpPlaylistMainScreen.add_Element(media);
                            AddTbInGrid(MainScreen, media.MediaName, totalCount);
                            ++totalCount;
                        }
                        ++j;
                    }
                    ++i;
                }

                if (tmpPlaylistMainScreen.getActualMedia().genres.Count() > 0)
                {
                    IEnumerable<Media> orderingQuery = null;
                    i = 0;
                    MainScreen.Children.Clear();
                    MainScreen.RowDefinitions.Clear();

                    orderingQuery =
            from media in tmpPlaylistMainScreen.PlDatas
            where media.mediaType == Media.MediaType.MUSIC
            orderby media.genres ascending
            select media;
                    OrderByStatus = EOrderBy.ASC;
                    foreach (Media media in orderingQuery)
                    {
                        AddTbInGrid(MainScreen, media.MediaName, i);
                        ++i;
                    }
                }
            }
        }

        private void MediaScreen_MediaOpened(object sender, RoutedEventArgs e)
        {
            _position = new TimeSpan(10);
            if ((currentPlaylist.getActualMedia().mediaType == Media.MediaType.VIDEO) || (currentPlaylist.getActualMedia().mediaType == Media.MediaType.MUSIC))
            _position = MediaScreen.NaturalDuration.TimeSpan;
            ProgressionMedia.Minimum = 0;
            ProgressionMedia.Maximum = _position.TotalSeconds;
        }

        private void DescSort_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<Media> orderingQuery = null;
            int i = 0;

            MainScreen.Children.Clear();
            MainScreen.RowDefinitions.Clear();
            if (tmpPlaylistMainScreen != null)
            {
                if (OrderByStatus == EOrderBy.ASC)
                {
                    orderingQuery =
            from media in tmpPlaylistMainScreen.PlDatas
            where media.mediaType == Media.MediaType.MUSIC
            orderby media.MediaName descending
            select media;
                    OrderByStatus = EOrderBy.DESC;
                    DescSort.Content = "A->Z";
                }
                else
                {
                    orderingQuery =
            from media in tmpPlaylistMainScreen.PlDatas
            where media.mediaType == Media.MediaType.MUSIC
            orderby media.MediaName ascending
            select media;
                    OrderByStatus = EOrderBy.ASC;
                    DescSort.Content = "Z->A";
                }
                foreach (Media med in orderingQuery)
                {
                    AddTbInGrid(MainScreen, med.MediaName, i);
                    ++i;
                }
            }
        }

        private void ProgressionMedia_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (MediaScreen.Position.ToString().Contains("."))
            TotalTimeMedia.Content = MediaScreen.Position.ToString().Remove(MediaScreen.Position.ToString().LastIndexOf("."));
        }

        private void ProgressionMedia_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var tmp = sender as Slider;
            MediaScreen.Position = TimeSpan.FromSeconds(tmp.Value);
        }

        private void VolumeBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var tmp = sender as Slider;

            MediaScreen.Volume = tmp.Value / 100;
        }

        private void RepeatButton_Click(object sender, RoutedEventArgs e)
        {
            switch (LectureStatus)
            {
                case ELectureTypeStatus.NOLOOP:
                    LectureStatus = ELectureTypeStatus.LOOPALL;
                    RepeatButton.Content = "repAll";
                    break;
                case ELectureTypeStatus.LOOPALL:
                    LectureStatus = ELectureTypeStatus.LOOPONE;
                    RepeatButton.Content = "repOne";
                    break;
                case ELectureTypeStatus.LOOPONE:
                    LectureStatus = ELectureTypeStatus.NOLOOP;
                    RepeatButton.Content = "retNone";
                    break;
            }
        }

        private void PlaylistName_TextInput(object sender, TextCompositionEventArgs e)
        {
            MessageBox.Show(PlaylistName.Text);
        }

        private void LibraryPath_Loaded(object sender, RoutedEventArgs e)
        {
            int i = 0;
            var tmp = sender as ComboBox;
            foreach (string s in lib.SavedLibraryPath)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = s;
                if (!CurrentPathLibrary.Items.Contains(item))
                    CurrentPathLibrary.Items.Add(item);
                ++i;
            }
        }

        private void BrowsePath_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folder = new System.Windows.Forms.FolderBrowserDialog();

            System.Windows.Forms.DialogResult result = folder.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = folder.SelectedPath.ToString();
                lib.AddLibrary(folder.SelectedPath.ToString());
                CurrentPathLibrary.Items.Add(item);
                lib.SaveLibraries();
            }

        }

        private void CurrentPathLibrary_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string[] Files;
            string[] DirectoriesInFolder;
            var tmp = sender as ComboBox;
            var selected = ((ComboBox)e.Source).SelectedValue;

            string newPath = selected.ToString();
            newPath = newPath.Substring(newPath.LastIndexOf(": ") + 2);
            if (System.IO.Directory.Exists(newPath))
            {
                if ((currentPlaylist.PlDatas.Count() > 0) && (currentPlaylist.getActualMedia().mediaType != Media.MediaType.VIDEO))
                {
                    MediaScreen.Visibility = System.Windows.Visibility.Hidden;
                    MainScreen.Visibility = System.Windows.Visibility.Visible;
                }
                MainScreen.Background = Brushes.White;
                Files = System.IO.Directory.GetFiles(newPath);
                DirectoriesInFolder = System.IO.Directory.GetDirectories(newPath);
                MainScreen.Children.Clear();
                MainScreen.RowDefinitions.Clear();
                int i = 0;
                int totalCount = 0;
                while (i < DirectoriesInFolder.Count())
                {
                    if (i > 0)
                        Files = System.IO.Directory.GetFiles(DirectoriesInFolder[i]);
                    int j = 0;
                    while (j < Files.Count())
                    {
                        Media media = new Media(Files[j]);
                        if ((media.mediaType != Media.MediaType.OTHER) && (media.mediaType != Media.MediaType.PLAYLIST))
                        {
                            tmpPlaylistMainScreen.add_Element(media);
                            AddTbInGrid(MainScreen, media.MediaName, totalCount);
                            ++totalCount;
                        }
                        ++j;
                    }
                    ++i;
                }
            }
            else
            {
                MessageBox.Show("The Folder doesn't exist.");
                return;
            }
        }

        private void ClickOnVideo(object sender, MouseButtonEventArgs e)
        {
            MainScreen.Background = Brushes.White;
            string[] MusicFiles;
            string[] DirectoriesInFolder;

            if (System.IO.Directory.Exists(Properties.Resources.VideoFolderPath))
            {
                if ((currentPlaylist.PlDatas.Count() > 0) && (currentPlaylist.getActualMedia().mediaType != Media.MediaType.VIDEO))
                {
                    MediaScreen.Visibility = System.Windows.Visibility.Hidden;
                    MainScreen.Visibility = System.Windows.Visibility.Visible;
                }
                CurrentPathLibrary.Text = Properties.Resources.VideoFolderPath;
                MainScreen.Background = Brushes.White;
                MusicFiles = System.IO.Directory.GetFiles(Properties.Resources.VideoFolderPath);
                DirectoriesInFolder = System.IO.Directory.GetDirectories(Properties.Resources.VideoFolderPath);
                MainScreen.Children.Clear();
                MainScreen.RowDefinitions.Clear();
                int i = 0;
                int totalCount = 0;
                while (i < DirectoriesInFolder.Count())
                {
                    if (i > 0)
                    MusicFiles = System.IO.Directory.GetFiles(DirectoriesInFolder[i]);
                    int j = 0;
                    while (j < MusicFiles.Count())
                    {
                        Media media = new Media(MusicFiles[j]);
                        if ((media.mediaType != Media.MediaType.OTHER) && (media.mediaType != Media.MediaType.PLAYLIST))
                        {
                        tmpPlaylistMainScreen.add_Element(media);
                        AddTbInGrid(MainScreen, media.MediaName, totalCount);
                        ++totalCount;
                        }
                        ++j;
                    }
                    ++i;
                }
            }
            else
            {
                MessageBox.Show("The Folder doesn't exist.");
                return;
            }

        }


    }
}
