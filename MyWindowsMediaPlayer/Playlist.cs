﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayer
{
    public class Playlist
    {
        public List<Media> PlDatas;
        public int currentlyPlayedIndex;
        public string name { get; set; }
        public Playlist()
        {
            PlDatas = new List<Media>();
            currentlyPlayedIndex = 0;
        }

        public void add_Element(Media elem)
        {
            if (elem != null)
                PlDatas.Add(elem);
            else
                MessageBox.Show("Error: null element");
        }
        public int PlaylistCount()
        {
            return (PlDatas.Count());
        }

        public Media getMedia(string name)
        {
            int i = 0;

            foreach (Media elem in PlDatas)
            {
                if (elem.MediaName == name)
                {
                    currentlyPlayedIndex = i;
                    return (elem);
                }
                ++i;
            }
            return (null);
        }
        public Media getActualMedia()
        {
            return (PlDatas[currentlyPlayedIndex]);
        }
        public Uri getNextSource(MediaElement elem)
        {
            if (PlDatas.Count > 1 && PlDatas.Count > (currentlyPlayedIndex + 1))
            {
                currentlyPlayedIndex += 1;
                return (PlDatas[currentlyPlayedIndex].GetSource());
            }
            return (null);
        }

        public Uri getPrevSource(MediaElement elem)
        {
            if (PlDatas.Count > 1 && currentlyPlayedIndex != 0)
            {
                currentlyPlayedIndex -= 1;
                return (PlDatas[currentlyPlayedIndex].GetSource());
            }
            return (null);
        }
        
        public void SavePlaylist()
        {
            string filePath;

            filePath = name;
            if (System.IO.Directory.Exists(Properties.Resources.PlayListFolderPath) == false)
            {
                System.IO.Directory.CreateDirectory(Properties.Resources.PlayListFolderPath);
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<Media>));
            FileStream stream = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.None);

            serializer.Serialize(stream, this.PlDatas);
            stream.Close();
        }
         
        public void LoadPlaylist(string path)
        {
            if (File.Exists(path) == true)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<Media>));
                FileStream stream = File.OpenRead(path);

                this.PlDatas = (List<Media>)serializer.Deserialize(stream);
                stream.Close();
                name = path.Substring(path.LastIndexOf("\\") + 1);
                name = name.Remove((name.Count() - 4));
                int i = 0;
                while ( i < PlDatas.Count())
                {
                                    if (!System.IO.File.Exists(PlDatas[i].InitialFileName))
                {
                    MessageBox.Show("File: " + PlDatas[i].MediaName + " not found. Maybe it was moved or deleted ?");
                    RemoveAt(PlDatas[i].MediaName);
                    SavePlaylist();
                }
                                    ++i;
                }
            }
        }
        public void RemoveAt(string filename)
        {
            int index = 0;
            foreach (Media media in PlDatas)
            {
                if (filename == media.MediaName)
                {
                    PlDatas.RemoveAt(index);
                    return;
                }
                ++index;
            }
        }

    }
}
