﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using TagLib;
using System.Drawing;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MyWindowsMediaPlayer
{
    public class Media
    {
            public enum MediaType
            {
                OTHER,
                MUSIC,
                VIDEO,
                IMAGE,
                PLAYLIST
            };

            //public MediaElement InitialMedia { get; set; }
            public string MediaName { get; set; }
            public string FilePath { get; set; }
            public string InitialFileName { get; set; }
            public string Extension { get; set; }
            public MediaType mediaType { get; set; }
            public uint trackNb { get; set; }
            public string[] artists { get; set; }
            public string[] genres { get; set; }
            public string album { get; set; }
            public int secondes { get; set; }
            public int minutes { get; set; }
            public int hours { get; set; }
            public byte[] imgDatas { get; set; }
            //public string imagePath { get; set; }
            //public TagLib.IPicture[] images { get; set; }

            public Media()
            {

            }

            public Media(string FileName)
            {
                string FinalFileName = FileName;
                MediaElement InitialMedia = new MediaElement();

                try
                {
                    //This part retrieve all basics informations about the opened file
//                filename = dial.FileName;
                InitialMedia.Source = new Uri(FileName);
                    InitialFileName = FileName;
                FinalFileName = FinalFileName.Substring(FinalFileName.LastIndexOf("\\") + 1);
                int found = FinalFileName.LastIndexOf('.');
                Extension = FinalFileName.Substring(found);
                MediaName = FinalFileName.Remove(found);
                FilePath = InitialMedia.Source.ToString();
                mediaType = CheckExt();

                //This part is about retrieving tags with TagLib
                if ((mediaType != MediaType.OTHER) && (mediaType != MediaType.PLAYLIST) && (mediaType != MediaType.IMAGE))
                    {
                TagLib.File Tags = TagLib.File.Create(FileName);
                    if ((FileName = Tags.Tag.Title) == null)
                        FileName = FinalFileName.Remove(found, 4);
                    trackNb = Tags.Tag.Track;
                    album = Tags.Tag.Album;
                    if ((artists = Tags.Tag.Performers) == null)
                        artists[0] = "";
                    minutes = Tags.Properties.Duration.Minutes;
                    secondes = Tags.Properties.Duration.Seconds;
                    genres = Tags.Tag.Genres;
                    hours = Tags.Properties.Duration.Hours;
                     //images = Tags.Tag.Pictures;
                    if (Tags.Tag.Pictures.Count() != 0)
                    {
                        imgDatas = Tags.Tag.Pictures[0].Data.Data;
                    }
                    else
                        imgDatas = null;
                    }
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("File not found.");
                }
            }

        public MediaType CheckExt()
        {
            if (Properties.Resources.AudioSupport.Contains(Extension))
                    return (MediaType.MUSIC);
            else if (Properties.Resources.ImageSupport.Contains(Extension))
                    return (MediaType.IMAGE);
            else if (Properties.Resources.VideoSupport.Contains(Extension))
                    return (MediaType.VIDEO);
            else if (Extension.Equals(Properties.Resources.PlaylistExtension))
                    return (MediaType.PLAYLIST);
            return (MediaType.OTHER);
        }
        public Uri GetSource()
        {
            return (new Uri(InitialFileName));
        }

    }
}
